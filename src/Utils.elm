module Utils exposing (maybeTypeMap)


maybeTypeMap : Maybe a -> b -> (a -> b) -> b
maybeTypeMap maybe newNothing newJust =
    case maybe of
        Just a ->
            newJust a

        Nothing ->
            newNothing
