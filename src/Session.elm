module Session exposing (Session, init, isAuthenticated)

import Browser.Navigation as Navigation
import User exposing (User)
import Utils


type Authenticated
    = LoggedIn User
    | Guest


type alias Session =
    { user : Authenticated
    , key : Navigation.Key
    }


authenticatedFromMaybe : Maybe User -> Authenticated
authenticatedFromMaybe maybeUser =
    Utils.maybeTypeMap maybeUser Guest LoggedIn


init : Maybe User -> Navigation.Key -> Session
init maybeUser key =
    { user = authenticatedFromMaybe maybeUser
    , key = key
    }


isAuthenticated : Session -> Bool
isAuthenticated session =
    case session.user of
        LoggedIn _ ->
            True

        Guest ->
            False
