module Page.ExamplePage exposing (Model, Msg, init, update, view)

import Element exposing (Element, text)
import Page.Status exposing (Status(..))
import Session exposing (Session)


type alias Model =
    { session : Session
    , pageName : Status String
    }


init : Session -> String -> ( Model, Cmd Msg )
init session name =
    ( { session = session, pageName = Loaded name }, Cmd.none )


type Msg
    = GotExamplePage


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotExamplePage ->
            ( model, Cmd.none )


listView : String -> Element Msg
listView pageName =
    text pageName


view : Model -> { title : String, content : Element Msg }
view model =
    Page.Status.view "Example Page" model.pageName listView
