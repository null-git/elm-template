module Page.Blank exposing (view)

import Element exposing (Element, none)


view : { title : String, content : Element msg }
view =
    { title = "blank"
    , content = none
    }
