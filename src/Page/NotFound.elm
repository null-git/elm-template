module Page.NotFound exposing (view)

import Element exposing (Element, text)


view : { title : String, content : Element msg }
view =
    { title = "404"
    , content = text "Not Found"
    }
