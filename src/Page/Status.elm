module Page.Status exposing (Status(..), view)

import Element exposing (Element, text)
import Http
import Ui.Layout exposing (contentView)


type Status a
    = Loading String
    | Loaded a
    | Failed Http.Error


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl url ->
            "Malformed Url: " ++ url

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network Error"

        Http.BadStatus status ->
            "Bad Status: " ++ String.fromInt status

        Http.BadBody message ->
            "Malformed Body: " ++ message


view : String -> Status a -> (a -> Element msg) -> { title : String, content : Element msg }
view title statusContent loadedView =
    let
        getPage =
            case statusContent of
                Loading info ->
                    contentView (text info)

                Failed error ->
                    contentView (text (errorToString error))

                Loaded data ->
                    contentView (loadedView data)
    in
    { title = title, content = getPage }
