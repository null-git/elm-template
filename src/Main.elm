module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Navigation
import Html
import Json.Decode exposing (Value)
import Page
import Page.Blank
import Page.ExamplePage as ExamplePage
import Page.NotFound
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)



-- Model


type Model
    = Redirect Session
    | NotFound Session
    | ExamplePage ExamplePage.Model


init : flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init _ url key =
    changeRouteTo
        (Route.fromUrl url)
        (Redirect (Session.init Nothing key))


toSession : Model -> Session
toSession model =
    case model of
        Redirect session ->
            session

        NotFound session ->
            session

        ExamplePage subModel ->
            subModel.session



-- View


view : Model -> Document Msg
view model =
    let
        viewPage page toMsg config =
            let
                { title, body } =
                    Page.view page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Redirect _ ->
            Page.view Page.Other Page.Blank.view

        NotFound _ ->
            Page.view Page.Other Page.NotFound.view

        ExamplePage subModel ->
            viewPage Page.Other GotExamplePageMsg (ExamplePage.view subModel)



-- Update


type Msg
    = UrlRequested Browser.UrlRequest
    | UrlChanged Url
    | GotExamplePageMsg ExamplePage.Msg


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just (Route.ExamplePage pageName) ->
            ExamplePage.init session pageName
                |> updateWith ExamplePage GotExamplePageMsg

        Just _ ->
            ( model, Cmd.none )


{-| Creates a new Main Model and Msg from a Page's Model and Msg.
I.e. Main.Model.ExamplePage vom Page.ExamplePage.{Model,Msg}
-}
updateWith : (subModel -> Model) -> (subMsg -> Msg) -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( UrlRequested urlRequested, _ ) ->
            case urlRequested of
                Browser.Internal url ->
                    case url.fragment of
                        Just _ ->
                            ( model
                            , Navigation.pushUrl
                                (.key (toSession model))
                                (Url.toString url)
                            )

                        Nothing ->
                            ( model, Cmd.none )

                Browser.External href ->
                    ( model
                    , Navigation.load href
                    )

        ( UrlChanged urlChanged, _ ) ->
            changeRouteTo (Route.fromUrl urlChanged) model

        ( GotExamplePageMsg subMsg, ExamplePage subModel ) ->
            ExamplePage.update subMsg subModel
                |> updateWith ExamplePage GotExamplePageMsg

        ( _, _ ) ->
            -- Ignore messages for the wrong page.
            ( model, Cmd.none )



-- Main


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


main : Program Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }
