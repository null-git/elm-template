module Ui.Colors exposing (background, blue, white)

import Element exposing (Color, rgb255)



-- Colors


background : Color
background =
    white


white : Color
white =
    rgb255 255 255 255


blue : Color
blue =
    rgb255 26 115 232
