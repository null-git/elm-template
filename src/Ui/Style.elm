module Ui.Style exposing
    ( fontHeader
    , fontMenu
    , fontSmall
    , spaceLg
    , spaceMd
    , spaceSm
    , spaceXl
    )

import Element exposing (Attribute)
import Element.Font as Font



-- Spacing


spaceMd : Int
spaceMd =
    12


spaceSm : Int
spaceSm =
    spaceMd // 2


spaceLg : Int
spaceLg =
    spaceMd * 2


spaceXl : Int
spaceXl =
    spaceMd * 4



-- Font


fontHeader : List (Attribute msg)
fontHeader =
    [ Font.size 32, Font.bold ]


fontMenu : Attribute msg
fontMenu =
    Font.size 24


fontSmall : Attribute msg
fontSmall =
    Font.size 14
