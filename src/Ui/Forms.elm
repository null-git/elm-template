module Ui.Forms exposing
    ( button
    , passwordField
    , textField
    , usernameField
    )

import Element exposing (Attribute, Element, centerX, el, fill, maximum, padding, spacing, text, width)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Ui.Colors exposing (blue, white)
import Ui.Style exposing (spaceMd)


textField : String -> String -> (String -> msg) -> Element msg
textField label input msg =
    Input.text
        []
        { label = Input.labelAbove [] (text label)
        , placeholder = Nothing
        , text = input
        , onChange = msg
        }


usernameField : String -> String -> (String -> msg) -> Element msg
usernameField label input msg =
    Input.username
        []
        { label = Input.labelAbove [] (text label)
        , placeholder = Nothing
        , text = input
        , onChange = msg
        }


passwordField : String -> String -> (String -> msg) -> Element msg
passwordField label input msg =
    Input.currentPassword
        []
        { label = Input.labelAbove [] (text label)
        , placeholder = Nothing
        , text = input
        , onChange = msg
        , show = False
        }


button : String -> msg -> Element msg
button label msg =
    Input.button
        [ Background.color blue, Font.color white, padding spaceMd, centerX ]
        { label = text label
        , onPress = Just msg
        }
