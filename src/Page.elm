module Page exposing (Page(..), view)

import Browser exposing (Document)
import Element exposing (Element, centerX, column, el, fill, height, layout, text, width)
import Element.Background as Background
import Html exposing (Html)
import Route exposing (Route)
import Ui.Colors exposing (background)
import Ui.Layout exposing (container, headerView)


type Page
    = Other


view : Page -> { title : String, content : Element msg } -> Document msg
view page { title, content } =
    { title = title
    , body =
        [ layout [ Background.color background ] <|
            el [ height fill, width fill, centerX ]
                (column
                    container
                    [ viewHeader title, content ]
                )
        ]
    }


viewHeader : String -> Element msg
viewHeader title =
    headerView (text title)


isActive : Page -> Route -> Bool
isActive page route =
    case ( page, route ) of
        _ ->
            False
